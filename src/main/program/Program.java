package program;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import genereted.Category;
import genereted.ColorType;
import genereted.Product;
import genereted.Store;
import genereted.Subcategory;

public class Program {

  private static Object unmarshal(String file) throws XMLStreamException, JAXBException {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    InputStream stream = classLoader.getResourceAsStream(file);
    XMLInputFactory xif = XMLInputFactory.newFactory();
    XMLStreamReader xsr = xif.createXMLStreamReader(stream);

    do
      xsr.nextTag();
    while (!xsr.getLocalName().equals("store"));

    JAXBContext jc = JAXBContext.newInstance(Store.class);
    Unmarshaller unmarshaller = jc.createUnmarshaller();
    JAXBElement<Store> jb = unmarshaller.unmarshal(xsr, Store.class);
    
    xsr.close();

    Store store = jb.getValue();
    return store;
  }

  private static void printStore(Store store) {
    ArrayList<Category> categoryList = new ArrayList<Category>();
    categoryList = (ArrayList<Category>) store.getCategory();
    StringBuilder stringBuilder = new StringBuilder();
    for (Category cat : categoryList) {
      stringBuilder.append("���������: ").append(cat.getName()).append("\n");
      ArrayList<Subcategory> subcategoryList = (ArrayList<Subcategory>) cat.getSubcategory();
      for (Subcategory subcat : subcategoryList) {
        stringBuilder.append(" ������������: ").append(subcat.getName()).append("\n");
        ArrayList<Product> productList = (ArrayList<Product>) subcat.getProduct();
        for (Product product : productList) {
          stringBuilder.append("  ��������: ").append(product.getName())
              .append("\n  �������������: ").append(product.getProducer()).append("\n  ������: ")
              .append(product.getModel()).append("\n  ����: ").append(product.getColor())
              .append("\n  ���� ������������: ").append(product.getManufactureDate())
              .append("\n  ����: ").append(product.getPrice()).append("\n  ���������� �� ������: ")
              .append(product.getAmount()).append("\n\n");
        }
      }
    }
    System.out.println(stringBuilder);
  }

  private static void marshal(JAXBElement<?> je)
      throws XMLStreamException, JAXBException, DatatypeConfigurationException, IOException {

    XMLOutputFactory factory = XMLOutputFactory.newInstance();
    XMLStreamWriter writer = factory.createXMLStreamWriter(System.out);
    writer.writeStartDocument();

    JAXBContext jc = JAXBContext.newInstance(Store.class);
    Marshaller marshaller = jc.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
    marshaller.marshal(je, writer);

    writer.writeEndDocument();
    writer.flush();
    writer.close();
  }

  private static XMLGregorianCalendar localDatetoGregorian(LocalDate date)
      throws DatatypeConfigurationException {
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.set(date.getYear(), date.getMonthValue(), date.getDayOfMonth());
    XMLGregorianCalendar xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
    return xmlCal;
  }

  private static void convertJSONtoXML(String json) throws JsonParseException, JsonMappingException,
      IOException, XMLStreamException, JAXBException, DatatypeConfigurationException {
    ObjectMapper mapper = new ObjectMapper();
    Product product = mapper.readValue(json, Product.class);

    QName root = new QName("product");
    JAXBElement<Product> je = new JAXBElement<Product>(root, Product.class, product);
    marshal(je);
  }

  private static String convertXMLtoJSON(String file)
      throws XMLStreamException, JAXBException, JsonProcessingException {
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    InputStream stream = classLoader.getResourceAsStream(file);
    XMLInputFactory xif = XMLInputFactory.newFactory();
    XMLStreamReader xsr = xif.createXMLStreamReader(stream);

    do
      xsr.nextTag();
    while (!xsr.getLocalName().equals("product"));

    JAXBContext jc = JAXBContext.newInstance(Product.class);
    Unmarshaller unmarshaller = jc.createUnmarshaller();
    JAXBElement<Product> jb = unmarshaller.unmarshal(xsr, Product.class);
    xsr.close();

    Product product = jb.getValue();

    ObjectMapper mapper = new ObjectMapper();
    String json = mapper.writeValueAsString(product);

    return json;
  }

  public static void main(String[] args) {
    // 2 task
    String file = "store.xml";
    
    try {
      // unmarshaling
      Store store1 = (Store) unmarshal(file);
      printStore(store1);

      // marshaling
      Product product1 =
          new Product("acer", "sd234", localDatetoGregorian(LocalDate.of(2010, 01, 01)),
              ColorType.BROWN, BigDecimal.valueOf(5544), BigInteger.valueOf(234), "folloe");
      Product product2 =
          new Product("saar", "kf984", localDatetoGregorian(LocalDate.of(2011, 11, 01)),
              ColorType.RED, BigDecimal.valueOf(344), BigInteger.valueOf(0), "loord");
      Product product3 =
          new Product("ggool", "jg099", localDatetoGregorian(LocalDate.of(2009, 11, 01)),
              ColorType.WHITE, BigDecimal.valueOf(2233), BigInteger.valueOf(33), "folloe");

      Subcategory subcategory1 = new Subcategory();
      subcategory1.setName("��������");
      subcategory1.getProduct().add(product1);
      subcategory1.getProduct().add(product2);

      Subcategory subcategory2 = new Subcategory();
      subcategory2.setName("��������");
      subcategory2.getProduct().add(product3);

      Category category = new Category();
      category.setName("�����������");
      category.getSubcategory().add(subcategory1);
      category.getSubcategory().add(subcategory2);

      Store store = new Store();
      store.setName("�������");
      store.getCategory().add(category);

      QName root = new QName("store");
      JAXBElement<Store> je = new JAXBElement<Store>(root, Store.class, store);
      marshal(je);

   // 3 task
      String json3 = convertXMLtoJSON(file);
      System.out.println("\n\ntask 3 " + json3);
      convertJSONtoXML(json3);
    } catch (XMLStreamException | JAXBException | DatatypeConfigurationException | IOException e) {
      e.printStackTrace();
    } 
  }
}
